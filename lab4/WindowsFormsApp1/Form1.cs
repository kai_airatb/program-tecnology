﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public delegate void DelAsyncEv();
    public delegate void DelParOpts(ParallelOptions parallelOptions);

    public partial class Form1 : Form
    {
        // ПРЯТКИ
        public event DelParOpts onRectHome;
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private CancellationTokenSource cancelToken = new CancellationTokenSource();

        List<Drawable> elements = new List<Drawable>();
        MyRectangle rect;
        public Form1()
        {
            InitializeComponent();

            Rectangle size = this.ClientRectangle;
            rect = new MyRectangle(size.Width / 2, size.Height / 2);
            rect.onHomeAsync += runCirles;
            elements.Add(rect);
            for (int i = 0; i < 4; ++i)
            {
                MyСircle el = new MyСircle(size, i);
                el.onMoveEnd += MoveEnd;
                onRectHome += new DelParOpts(el.Run);
                elements.Add(el);
            }
        }

        public void runCirles()
        {
            ParallelOptions parOpts = new ParallelOptions();
            parOpts.CancellationToken = cancelToken.Token;
            ThreadPool.QueueUserWorkItem(new WaitCallback((object state) =>
            {
                Delegate[] list = onRectHome.GetInvocationList();

                Parallel.Invoke(
                    () => ((DelParOpts)list[0]).Invoke(parOpts),
                    () => ((DelParOpts)list[1]).Invoke(parOpts),
                    () => ((DelParOpts)list[2]).Invoke(parOpts),
                    () => ((DelParOpts)list[3]).Invoke(parOpts)
                );
            }));
        }

        public void MoveEnd(int id)
        {
            this.Invoke(new Action(() => {
                this.Text = string.Format("Шарик {0} остановился", id);
            }));
        }

        private async void buttonStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                timer.Tick += new EventHandler((Object s, EventArgs ea) => Invalidate());
                timer.Interval = 100;
                timer.Start();
                await rect.GoHomeAsync();
                Console.WriteLine("HOME");
            } 
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Drawable el in elements)
                el.Draw(e.Graphics);
        }

        private void сancel_Click(object sender, EventArgs e)
        {
            cancelToken.Cancel();
        }
    }
}

