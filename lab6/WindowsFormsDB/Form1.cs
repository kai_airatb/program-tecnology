﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void предметыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
           
            this.tableAdapterManager.UpdateAll(this._Student_DataSet);

        }

        private void предметыBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            
            this.tableAdapterManager.UpdateAll(this._Student_DataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_Student_DataSet.Специальности". При необходимости она может быть перемещена или удалена.
            this.специальностиTableAdapter.Fill(this._Student_DataSet.Специальности);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_Student_DataSet.Предметы". При необходимости она может быть перемещена или удалена.
      

        }

        private void специальностиBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.специальностиBindingSource.EndEdit();
            this.tableAdapterManager1.UpdateAll(this._Student_DataSet);

        }
    }
}
