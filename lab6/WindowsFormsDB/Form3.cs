﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsDB
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void студентыBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.студентыBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this._Student_DataSet);

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_Student_DataSet.Специальности". При необходимости она может быть перемещена или удалена.
            this.специальностиTableAdapter.Fill(this._Student_DataSet.Специальности);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "_Student_DataSet.Студенты". При необходимости она может быть перемещена или удалена.
            this.студентыTableAdapter.Fill(this._Student_DataSet.Студенты);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            студентыBindingSource.MovePrevious();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            студентыBindingSource.MoveNext();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            студентыBindingSource.MoveFirst();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            студентыBindingSource.MoveLast();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            студентыBindingSource.AddNew();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            студентыBindingSource.RemoveCurrent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Validate();
            студентыBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(_Student_DataSet);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            new Form5().Show();
        }
    }
}
