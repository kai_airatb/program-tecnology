﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public delegate string DelAsyncEv();

    public partial class Form1 : Form
    {
        // ПРЯТКИ
        public event  DelAsyncEv onStart;
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        List<Drawable> elements = new List<Drawable>();
        MyRectangle rect;
        public Form1()
        {
            InitializeComponent();

            Rectangle size = this.ClientRectangle;
            rect = new MyRectangle(size.Width / 2, size.Height / 2);
            onStart += new DelAsyncEv(rect.GoHome);
            elements.Add(rect);
            for (int i = 0; i < 4; ++i)
            {
                MyСircle el = new MyСircle(size, i);
                rect.onHomeAsync += new DelAsyncEv(el.Run);
                el.onMoveEnd += MoveEnd;
                elements.Add(el);
            }
        }

        public void MoveEnd(int id)
        {
            this.Invoke(new Action(() => {
                this.Text = string.Format("Шарик {0} остановился", id);
            }));
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                timer.Tick += new EventHandler((Object s, EventArgs ea) => Invalidate());
                timer.Interval = 100;
                timer.Start();
                Task.Run(onStart.Invoke);
            } 
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Drawable el in elements)
                el.Draw(e.Graphics);
        }
    }
}
