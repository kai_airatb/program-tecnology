﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApp1
{

    class MyRectangle : Drawable
    {
        public event            DelAsyncEv onHomeAsync;

        private RectangleF      rect1;
        private RectangleF      rect2;
        private RectangleF      target;
        private bool            go;
        private Pen             pen;
        private Brush           brush;

        public MyRectangle(int x, int y)
        {
            int width   = 120;
            int height  = 80;

            this.go     = false;
            this.target = new RectangleF(x - width / 2, y - height / 2, width, height);
            this.rect1  = new RectangleF(x - width / 2, 0, width, height);
            this.rect2  = new RectangleF(x - width / 2, y * 2 - height, width, height);
            this.brush  = new SolidBrush(Color.Blue);
            this.pen    = Pens.Red;
        }

        public string GoHome()
        {
            this.go = true;
            if (rect1 != target)
            {
                while (rect1 != target)
                {
                    rect1.Y += 1;
                    Thread.Sleep(15);
                }
                ThreadPool.QueueUserWorkItem(new WaitCallback((object state) =>
                {
                    Parallel.ForEach(onHomeAsync.GetInvocationList(), del => {
                        ((DelAsyncEv)del).Invoke();
                    });
                }));
            }
            return ("Прямоугольник");
        }

        public void ChangeColor()
        {
            this.brush  = new SolidBrush(Color.Green);
        }

        public void Draw(Graphics dc)
        {
            dc.DrawRectangle(pen, target.X - 1, target.Y - 1, target.Width + 1, target.Height + 1);
            if (go)
            {
                dc.FillRectangle(brush, rect1);
            }
        }
    }
}
