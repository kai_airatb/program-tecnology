﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class MyEventArgs : EventArgs
    {
        public delegate void Callback();
    }

    public partial class Form1 : Form
    {
        public delegate void    StartEventHandler(); 
        public event            StartEventHandler onStart;
        private Timer           timer = new Timer();

        List<Drawable> elements = new List<Drawable>();
        public Form1()
        {
            InitializeComponent();

            Rectangle size = this.ClientRectangle;
            MyRectangle rect = new MyRectangle(size.Width / 2, size.Height / 2);
            onStart += rect.GoHome;
            elements.Add(rect);
            for (int i = 0; i < 4; ++i)
            {
                MyСircle el = new MyСircle(size, i);
                rect.onHome += el.Run;
                elements.Add(el);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                timer.Tick += new EventHandler((Object s, EventArgs ea) => Invalidate());
                timer.Interval = 100;
                timer.Start();
                onStart?.Invoke();
            } 
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Drawable el in elements)
                el.Draw(e.Graphics);
        }
    }
}
