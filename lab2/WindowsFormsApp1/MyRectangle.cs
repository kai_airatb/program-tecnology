﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApp1
{

    class MyRectangle : Drawable
    {
        public event            DelAsyncEv onHomeAsync;

        private RectangleF      rect;
        private RectangleF      target;
        private bool            go;
        private Pen             pen;
        private Brush           brush;

        public MyRectangle(int x, int y)
        {
            int width   = 120;
            int height  = 80;

            this.go     = false;
            this.target = new RectangleF(x - width / 2, y - height / 2, width, height);
            this.rect   = new RectangleF(x - width / 2, 0, width, height);
            this.brush  = new SolidBrush(Color.Blue);
            this.pen    = Pens.Red;
        }

        public string GoHome()
        {
            this.go = true;
            if (rect != target)
            {
                while (rect != target)
                {
                    rect.Y += 1;
                    Thread.Sleep(15);
                }
                Delegate[] delList = onHomeAsync.GetInvocationList();
                foreach(DelAsyncEv del in delList)
                     del.BeginInvoke(null, null);
            }
            return ("Прямоугольник");
        }

        public void ChangeColor()
        {
            this.brush  = new SolidBrush(Color.Green);
        }

        public void Draw(Graphics dc)
        {
            dc.DrawRectangle(pen, target.X - 1, target.Y - 1, target.Width + 1, target.Height + 1);
            if (go)
                dc.FillRectangle(brush, rect);
        }
    }
}
