﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Threading;

namespace WindowsFormsApp1
{
    class MyСircle : Drawable
    {
        private int        n;
        private int        dx;
        private int        dy;
        private Brush      brush;
        private Rectangle  wSize;
        private RectangleF rect;

        public MyСircle(Rectangle size, int n)
        {
            this.n = n;
            this.dx     = (n % 2) * (n > 1 ? -1 : 1);
            this.dy     = ((n + 1) % 2) * (n > 1 ? -1 : 1);
            this.wSize  = size;
            this.brush  = new SolidBrush(Color.BlueViolet);
            this.rect   = new RectangleF(size.Width / 2 + dx * 100 - 10,
                                         size.Height / 2 + dy * 100 - 10, 20, 20);
        }

        public string Run()
        {
            while (rect.X != 0 && rect.X != wSize.Width - 20 && 
                   rect.Y != 0 && rect.Y != wSize.Height - 20)
            {
                rect.X += dx;
                rect.Y += dy;
                Thread.Sleep(15);
            }
            return ("Круг" + n);
        }
        public void Draw(Graphics dc) => dc.FillEllipse(brush, rect);
    }
}
