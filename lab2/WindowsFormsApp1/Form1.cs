﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public delegate string DelAsyncEv();

    public partial class Form1 : Form
    {
        public event  DelAsyncEv onStart;
        private Timer timer = new Timer();

        List<Drawable> elements = new List<Drawable>();
        MyRectangle rect;
        public Form1()
        {
            InitializeComponent();

            Rectangle size = this.ClientRectangle;
            rect = new MyRectangle(size.Width / 2, size.Height / 2);
            onStart += new DelAsyncEv(rect.GoHome);
            elements.Add(rect);
            for (int i = 0; i < 4; ++i)
            {
                MyСircle el = new MyСircle(size, i);
                rect.onHomeAsync += new DelAsyncEv(el.Run);
                elements.Add(el);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                timer.Tick += new EventHandler((Object s, EventArgs ea) => Invalidate());
                timer.Interval = 100;
                timer.Start();
                IAsyncResult asyncResult = onStart.BeginInvoke(new AsyncCallback(Callback), null);
            } 
        }
        private void Callback(IAsyncResult asyncResult)
        {
            onStart.EndInvoke(asyncResult);
            Console.WriteLine("hello");
            rect.ChangeColor();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Drawable el in elements)
                el.Draw(e.Graphics);
        }
    }
}
