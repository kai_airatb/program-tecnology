﻿using lab8.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace lab8.Utilities{
    public class DatabaseUtility
    {
        readonly string connectionString = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source =|DataDirectory|\";
        readonly DataTable studentsDataTable = new DataTable("Студенты");
        readonly DataTable specialtiesDataTable = new DataTable("Специальности");
        readonly DataTable subjectsDataTable = new DataTable("Предметы");
        readonly DataTable marksDataTable = new DataTable("Оценки");
        OleDbDataAdapter studentsDataAdapter;
        OleDbDataAdapter specialtiesDataAdapter;
        OleDbDataAdapter marksDataAdapter;
        OleDbDataAdapter subjectsDataAdapter;

        public DatabaseUtility(string databaseFilename)        
        {            
            connectionString += databaseFilename;
            studentsDataAdapter = new OleDbDataAdapter("SELECT * from Студенты", connectionString);
            specialtiesDataAdapter = new OleDbDataAdapter("SELECT * from Специальности", connectionString);
            marksDataAdapter = new OleDbDataAdapter("SELECT * from Оценки", connectionString);
            subjectsDataAdapter = new OleDbDataAdapter("SELECT * from Предметы", connectionString);
            marksDataAdapter.UpdateCommand = new OleDbCommand("UPDATE Оценки SET [Оценка 1] = ?, [Оценка 2] = ?, [Средний балл] = ?  WHERE [Код студента] = ? AND [Код предмета 1] = ?;" + "", new OleDbConnection(connectionString));
            marksDataAdapter.UpdateCommand.Parameters.Add(new OleDbParameter { 
                DbType = DbType.Int32, 
                SourceColumn = "Оценка 1",
                ParameterName = "Оценка 1" 
            });
            marksDataAdapter.UpdateCommand.Parameters.Add(new OleDbParameter {
                DbType = DbType.Int32,
                SourceColumn = "Оценка 2",
                ParameterName = "Оценка 2"
            });
            marksDataAdapter.UpdateCommand.Parameters.Add(new OleDbParameter {
                DbType = DbType.Int32,
                SourceColumn = "Средний балл",
                ParameterName = "Средний балл"
            });
            marksDataAdapter.UpdateCommand.Parameters.Add(new OleDbParameter {
                DbType = DbType.Int32,
                SourceColumn = "Код студента",
                ParameterName = "Код студента"
            });
            marksDataAdapter.UpdateCommand.Parameters.Add(new OleDbParameter {
                DbType = DbType.Int32,
                SourceColumn = "Код предмета 1",
                ParameterName = "Код предмета 1"
            });

            studentsDataAdapter.Fill(studentsDataTable);
            specialtiesDataAdapter.Fill(specialtiesDataTable);
            subjectsDataAdapter.Fill(subjectsDataTable);
            marksDataAdapter.Fill(marksDataTable);
        }
        public Student[] getStudents(int subjectId)
        {
            var studentsIds = (from marks in marksDataTable.AsEnumerable()
                               where ((int)marks["Код предмета 1"] == subjectId
                               && (int)marks["Оценка 1"] <= 2 
                               || (int)marks["Код предмета 2"] == subjectId
                               && (int)marks["Оценка 2"] <= 2)
                               select (int)marks["Код студента"]).ToList();

            var students = (
                from student in studentsDataTable.AsEnumerable()
                orderby student["ФИО"].ToString() ascending
                where studentsIds.Contains((int)student["Код студента"])
                select new Student
                {
                    Id = (int)student["Код студента"],
                    Name = (string)student["ФИО"],
                    Gender = (string)student["Пол"],
                    Address = (string)student["Адрес"],
                    Phone = (string)student["Телефон"],
                    Passport = (string)student["Паспортные данные"],
                    CardNumber = (int)student["Номер зачетки"],
                    GroupNumber = (string)student["Группа"],
                    Year = (int)student["Курс"],
                    SpecialtyId = (int)student["Код специальности"],
                    SubjectId = (int)subjectId
                }
            ); ;

            return students.ToArray();
        }

        public Subjects[] getSubjects()
        {
            var subjects = (
                from subject in subjectsDataTable.AsEnumerable()
                orderby subject["Код предмета"].ToString() ascending
                select new Subjects
                {
                    Id = (int)subject["Код предмета"],
                    Name = (string)subject["Название предмета"],
                    Description = (string)subject["Описание предмета"]
                }
            );
            return subjects.ToArray();
        }

        public void setMark(int studentId, int subjectId, int mark)
        {
            var studs = from m in marksDataTable.AsEnumerable()
                        where m["Код студента"].ToString() == studentId.ToString()
                        select m;
            var stud = studs.First();
            if (subjectId == stud.Field<int>("Код предмета 1"))
                stud.SetField("Оценка 1", mark);
            else if (subjectId == stud.Field<int>("Код предмета 2"))
                stud.SetField("Оценка 2", mark);
            stud.SetField("Средний балл", (stud.Field<int>("Оценка 1") + stud.Field<int>("Оценка 2")) / 2);
            marksDataAdapter.Update(studs.ToArray());
        }
    }
}