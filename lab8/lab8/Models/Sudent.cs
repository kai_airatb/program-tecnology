﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab8.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Passport { get; set; }
        public int CardNumber { get; set; }
        public string GroupNumber { get; set; }
        public int Year { get; set; }
        public int SpecialtyId { get; set; }
        public int SubjectId { get; set; }
        
    }
}