﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lab8.Models{
    public class Marks
    {
        public int StudentId { get; set; }
        public string Date1 { get; set; }
        public string SubjectId1 { get; set; }
        public string Mark1 { get; set; }
        public string Date2 { get; set; }
        public string SubjectId2 { get; set; }
        public string Mark2 { get; set; }
        public string Middle { get; set; }

    }
}
