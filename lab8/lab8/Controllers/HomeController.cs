﻿using lab8.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab8.Controllers{
    public class HomeController: Controller
    {
        DatabaseUtility databaseUtility = new DatabaseUtility("Студенты.mdb");
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Subjects()
        {
            return View(databaseUtility.getSubjects());
        }
        public ActionResult Students(int subjectId)
        {
            return View(databaseUtility.getStudents(subjectId));
        }
        public ActionResult setMark(int studentId, int subjectId, int mark)
        {
            databaseUtility.setMark(studentId, subjectId, mark);
            return Redirect("Subjects");
        }
    }
}