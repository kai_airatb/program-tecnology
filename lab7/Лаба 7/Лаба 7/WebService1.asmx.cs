﻿using System;
using System.Linq;
using System.Web.Services;
using System.Data;
using System.Data.OleDb;

namespace Лаба_7
{
    /// <summary>
    /// Сводное описание для WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        /*Доступ к разным таблицам*/
        DataTable dt_students = new DataTable("Студенты");
        DataTable dt_marks = new DataTable("Оценки");
        DataTable dt_subjects = new DataTable("Предметы");
        DataTable dt_specialties = new DataTable("Специальности");

        public WebService1()
        {
            //Строка подключения к БД
            //Provider - с помощью чего будет осуществляться подключение к БД
            //Data Source - путь к БД
            //|DataDirectory| означает что искать нужно в рабочей директории
            string connectStr = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source = C:\Users\airat\Desktop\Лаба 7\Студенты.mdb";

            OleDbDataAdapter dataAdapterStudents = new OleDbDataAdapter("Select * from Студенты", connectStr);
            OleDbCommandBuilder studTable = new OleDbCommandBuilder(dataAdapterStudents);
            dataAdapterStudents.Fill(dt_students);

            OleDbDataAdapter dataAdapterMarks = new OleDbDataAdapter("Select * from Оценки", connectStr);
            OleDbCommandBuilder markTable = new OleDbCommandBuilder(dataAdapterMarks);
            dataAdapterMarks.Fill(dt_marks);

            OleDbDataAdapter dataAdapterSubjects = new OleDbDataAdapter("Select * from Предметы", connectStr);
            OleDbCommandBuilder subjectTable = new OleDbCommandBuilder(dataAdapterSubjects);
            dataAdapterSubjects.Fill(dt_subjects);

            OleDbDataAdapter dataAdapterSpecialties = new OleDbDataAdapter("Select * from Специальности", connectStr);
            OleDbCommandBuilder specialtiesTable = new OleDbCommandBuilder(dataAdapterSpecialties);
            dataAdapterSpecialties.Fill(dt_specialties);
        }

        /*
         * 1. По какой специальности обучается студент с заданнойпользователем фамилией и информацию о ней;
         * - [x] Список групп
         * - [x] Список студентов группы
         * - [x] по фио - id специальности
         * - [x] информация о специальности по id студента
         */

        [WebMethod]
        public string[] GetGroupList()
        {
            var groupList = from stud in dt_students.AsEnumerable()
                            orderby stud["Группа"] ascending
                            group stud by stud["Группа"].ToString() into g
                            select g.Key.ToString();

            return groupList.ToArray();
        }

        [WebMethod]
        public string[] GetFIOsByGroup(string groupNum)
        {
            var fioList = from stud in dt_students.AsEnumerable()
                          where stud["Группа"].ToString() == groupNum
                          select stud["ФИО"].ToString();

            return fioList.ToArray();
        }

        [WebMethod]
        public string GetSpecIDByFIO(string FIO)
        {
            var id = (from stud in dt_students.AsEnumerable()
                     where stud["ФИО"].ToString() == FIO
                     select stud["Код специальности"].ToString()).FirstOrDefault();

            return id;
        }

        [WebMethod]
        public string[] GetSpecInfoBySpecID(string SpecID)
        {
            var name = (from spec in dt_specialties.AsEnumerable()
                       where spec["Код специальности"].ToString() == SpecID
                       select spec["Наименование специальности"]).FirstOrDefault();

            var info = (from spec in dt_specialties.AsEnumerable()
                       where spec["Код специальности"].ToString() == SpecID
                       select spec["Описание специальности"]).FirstOrDefault();

            return new string[] { SpecID, name.ToString(), info.ToString() };
        }
    }
}
