﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.fioComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SpecId = new System.Windows.Forms.Label();
            this.SpecName = new System.Windows.Forms.Label();
            this.SpecInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // groupComboBox
            // 
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(12, 68);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(121, 21);
            this.groupComboBox.TabIndex = 0;
            this.groupComboBox.SelectedIndexChanged += new System.EventHandler(this.groupComboBox_SelectedIndexChanged);
            // 
            // fioComboBox
            // 
            this.fioComboBox.FormattingEnabled = true;
            this.fioComboBox.Location = new System.Drawing.Point(139, 68);
            this.fioComboBox.Name = "fioComboBox";
            this.fioComboBox.Size = new System.Drawing.Size(477, 21);
            this.fioComboBox.TabIndex = 1;
            this.fioComboBox.SelectedIndexChanged += new System.EventHandler(this.fioComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Номер группы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(136, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "ФИО";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(547, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "1. По какой специальности обучается студент с заданнойпользователем фамилией и ин" +
    "формацию о ней;";
            // 
            // SpecId
            // 
            this.SpecId.AutoSize = true;
            this.SpecId.Location = new System.Drawing.Point(13, 139);
            this.SpecId.Name = "SpecId";
            this.SpecId.Size = new System.Drawing.Size(106, 13);
            this.SpecId.TabIndex = 5;
            this.SpecId.Text = "Код специальности";
            // 
            // SpecName
            // 
            this.SpecName.AutoSize = true;
            this.SpecName.Location = new System.Drawing.Point(13, 167);
            this.SpecName.Name = "SpecName";
            this.SpecName.Size = new System.Drawing.Size(137, 13);
            this.SpecName.TabIndex = 6;
            this.SpecName.Text = "Название спациальности";
            // 
            // SpecInfo
            // 
            this.SpecInfo.AutoSize = true;
            this.SpecInfo.Location = new System.Drawing.Point(13, 196);
            this.SpecInfo.MaximumSize = new System.Drawing.Size(600, 300);
            this.SpecInfo.Name = "SpecInfo";
            this.SpecInfo.Size = new System.Drawing.Size(156, 13);
            this.SpecInfo.TabIndex = 7;
            this.SpecInfo.Text = "Информация о специльности";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 391);
            this.Controls.Add(this.SpecInfo);
            this.Controls.Add(this.SpecName);
            this.Controls.Add(this.SpecId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fioComboBox);
            this.Controls.Add(this.groupComboBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.ComboBox fioComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label SpecId;
        private System.Windows.Forms.Label SpecName;
        private System.Windows.Forms.Label SpecInfo;
    }
}

