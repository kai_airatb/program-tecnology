﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Лаба_7;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        WebService1 Web = new WebService1();
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupComboBox.Items.AddRange(Web.GetGroupList());
        }

        private void groupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox gcb = (ComboBox)sender;
            fioComboBox.Items.Clear();
            fioComboBox.Items.AddRange(Web.GetFIOsByGroup(gcb.SelectedItem.ToString()));
            this.SpecId.Text   = "Код специальности";
            this.SpecName.Text = "Название спациальности";
            this.SpecInfo.Text = "Информация о специльности";
        }

        private void fioComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox fcb = (ComboBox)sender;
            string specID = Web.GetSpecIDByFIO(fcb.SelectedItem.ToString());
            string[] info = Web.GetSpecInfoBySpecID(specID);
            this.SpecId.Text   = info[0];
            this.SpecName.Text = info[1];
            this.SpecInfo.Text = info[2];
        }
    }
}
