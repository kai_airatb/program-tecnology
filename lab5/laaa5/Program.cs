﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace laaa5
{
    class Student
    {
        public string Name;
        public int Mark;
        public string Surname;
    }
    class A
    {
        static List<Student> students = new List<Student>()
        {
            new Student {Surname="Дьяченко",   Name="Ярослав",   Mark=5,},
            new Student {Surname="Грязев",     Name="Куприян",   Mark=2,},
            new Student {Surname="Бахтеяров",  Name="Ермолай",   Mark=5,},
            new Student {Surname="Албычева",   Name="Наталия",   Mark=3,},
            new Student {Surname="Саблина",    Name="Антонина",  Mark=5,},
            new Student {Surname="Коротнева",  Name="Ольга",     Mark=2,},
        };
        static void Main()
        {
            string nom;
            do
            {
                Console.Clear();
                Console.WriteLine("Выберите пункт:");
                Console.WriteLine("1. Вывести список студентов.");
                Console.WriteLine("2. Вывести всех студентов, которые сдали тест на «5».");
                Console.WriteLine("3. Вывести рейтинг студентов, начиная с отличников.");
                Console.WriteLine("4. Определить, кого больше: отличников или двоечников.");
                Console.WriteLine("5. Разбить студентов на группы по первой букве фамилии по алфавиту.");
                Console.WriteLine("6. Найти разность между наибольшим и наименьшим значением набранных баллов.");
                Console.WriteLine("7. Добавить в список студентов еще одну попытку имеющегося студента сдать тест(с новыми баллами).");
                Console.WriteLine("8. Создать список результатов студентов по тесту.");
                Console.WriteLine("9. Выход из меню.");
                nom = Console.ReadLine();
                Console.WriteLine("");
                switch (nom)
                {
                    case "1": Zad1(); break;
                    case "2": Zad2(); break;
                    case "3": Zad3(); break;
                    case "4": Zad4(); break;
                    case "5": Zad5(); break;
                    case "6": Zad6(); break;
                    case "7": Zad7(); break;
                    case "8": Zad8(); break;
                    case "9": break;
                    default: Console.WriteLine("Введите число от 1 до 9!"); break;
                }
                if (nom != "9")
                {
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                }
            }
            while (nom != "9");
        }
        public static void Zad1()
        {
            var reit = from s in students orderby s.Name ascending select s;
            foreach (var i in reit)
                Console.WriteLine("{0,-12}{1,-12}{2,-2}", i.Name, i.Surname, i.Mark);
        }
        public static void Zad2()
        {
            var otlichniki = from s in students where s.Mark == 5 select s;
            foreach (var i in otlichniki)
                Console.WriteLine("{0,-12}{1,-12}{2,-2}", i.Name, i.Surname, i.Mark);
        }
        public static void Zad3()
        {
            var reit = from s in students orderby s.Mark descending select s;
            foreach (var i in reit)
                Console.WriteLine("{0,-12}{1,-12}{2,-2}", i.Name, i.Surname, i.Mark);
        }
        public static void Zad4()
        {
            if ((from s in students where s.Mark == 5 select s).Count() > (from s in students where s.Mark == 2 select s).Count())
                Console.WriteLine("Отличников больше");
            if ((from s in students where s.Mark == 5 select s).Count() < (from s in students where s.Mark == 2 select s).Count())
                Console.WriteLine("Двоечников больше");
            if ((from s in students where s.Mark == 5 select s).Count() == (from s in students where s.Mark == 2 select s).Count())
                Console.WriteLine("Отличников и двоечников одинаково");
        }
        public static void Zad5()
        {
            var groups = from s5 in students orderby s5.Surname ascending group s5 by s5.Surname[0] into g select new
                        {
                             Let = g.Key,
                             Names = from p in g orderby p.Surname ascending select p
                        };
            foreach (var group in groups)
            {
                Console.WriteLine("{0}:", group.Let);
                foreach (var i in group.Names)
                    Console.WriteLine("{0,-12}{1,-12}{2,-2}", i.Surname, i.Name, i.Mark);
            }
        }
        public static void Zad6()
        {
            var Diff = (from s6 in students select s6.Mark).Max() - (from s6 in students select s6.Mark).Min();
            Console.WriteLine("Разность набранных баллов - {0}", Diff);
        }
        public static void Zad7()
        {
            Console.WriteLine("Добавлено исправление оценки для Коротневой Ольги");
            students.Add(new Student { Surname = "Коротнева", Name = "Ольга", Mark = 5});
        }
        public static void Zad8()
        {
            var groups1 = from s8 in students orderby s8.Surname ascending group s8 by s8.Surname into g select new
                            {
                                Surname = g.Key,
                                Mark = (from p in g select p.Mark).Max()
                            };
            foreach (var group in groups1)
            {
                Console.WriteLine("{0}:", group.Surname);
                Console.WriteLine("{0}",  group.Mark);
            }
        }
    }
}
